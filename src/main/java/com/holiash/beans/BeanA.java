package com.holiash.beans;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanA implements BeanValidator, InitializingBean, DisposableBean {

  private String name;
  private int value;

  public BeanA(String name, int value) {
    System.out.println();
    System.out.println("BeanA init");
    this.name = name;
    this.value = value;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "BeanA{" +
      "name='" + name + '\'' +
      ", value=" + value +
      '}';
  }

  @Override
  public boolean validate() {
    System.out.println("BeanA validation");
    return name != null && value > 0;
  }

  @Override
  public void destroy() throws Exception {
    System.out.println("Destroy method in BeanA");
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    System.out.println("afterPropertiesSet method in BeanA");
  }
}
