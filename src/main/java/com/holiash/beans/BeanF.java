package com.holiash.beans;

public class BeanF implements BeanValidator {

  private String name;
  private int value;

  public BeanF(String name, int value) {
    System.out.println();
    System.out.println("BeanF init");
    this.name = name;
    this.value = value;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "BeanF{" +
      "name='" + name + '\'' +
      ", value=" + value +
      '}';
  }

  @Override
  public boolean validate() {
    System.out.println("Validating BeanF");
    return name != null && value > 0;
  }
}
