package com.holiash.beans;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE implements BeanValidator {

  private String name;
  private int value;

  public BeanE(String name, int value) {
    System.out.println();
    System.out.println("BeanE init");
    this.name = name;
    this.value = value;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "BeanE{" +
      "name='" + name + '\'' +
      ", value=" + value +
      '}';
  }

  @Override
  public boolean validate() {
    System.out.println("Validating BeanE");
    return name != null && value > 0;
  }

  @PostConstruct
  public void postConstructMethod() {
    System.out.println("postConstructMethod in BeanE");
  }

  @PreDestroy
  public void preDestroyMethod() {
    System.out.println("preDestroyMethod in BeanE");
  }
}
