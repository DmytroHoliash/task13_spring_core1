package com.holiash.beans;

public class BeanC implements BeanValidator {

  private String name;
  private int value;

  public BeanC(String name, int value) {
    System.out.println();
    System.out.println("BeanC init");
    this.name = name;
    this.value = value;
  }

  public void init() {
    System.out.println("Init method in BeanC");
  }

  public void destroy() {
    System.out.println("Destroy method in BeanC");
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "BeanC{" +
      "name='" + name + '\'' +
      ", value=" + value +
      '}';
  }

  @Override
  public boolean validate() {
    System.out.println("BeanC validation");
    return name != null && value > 0;
  }
}
