package com.holiash.beans;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class MyBeanPostProcessor implements BeanPostProcessor {

  public MyBeanPostProcessor() {
    System.out.println("MyBeanPostProcessor constructor()");
  }

  @Override
  public Object postProcessBeforeInitialization(Object bean, String beanName)
    throws BeansException {
    System.out.println("postProcessBeforeInitialization");
    System.out.println(" >>bean=" + bean + " beanName=" + beanName);
    return bean;
  }

  @Override
  public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
    System.out.println("postProcessAfterInitialization");
    System.out.println(" >>bean=" + bean + " beanName=" + beanName);
    if (bean instanceof BeanValidator) {
      if (((BeanValidator) bean).validate()) {
        System.out.println(" >>bean=" + bean + " is valid");
        return bean;
      }
    }
    return null;
  }
}
