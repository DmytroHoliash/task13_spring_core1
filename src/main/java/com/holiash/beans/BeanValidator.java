package com.holiash.beans;

public interface BeanValidator {

  boolean validate();
}
