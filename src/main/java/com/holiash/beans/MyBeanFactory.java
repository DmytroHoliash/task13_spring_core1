package com.holiash.beans;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

public class MyBeanFactory implements BeanFactoryPostProcessor {

  public void init() {
    System.out.println("Init MyBeanFactory");
  }

  @Override
  public void postProcessBeanFactory(
    ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
    BeanDefinition bf = configurableListableBeanFactory.getBeanDefinition("getBeanB");
    bf.setInitMethodName("postProcessorInit");
  }
}
