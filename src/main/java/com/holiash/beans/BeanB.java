package com.holiash.beans;

public class BeanB implements BeanValidator {

  private String name;
  private int value;

  public BeanB(String name, int value) {
    System.out.println();
    System.out.println("BeanB init");
    this.name = name;
    this.value = value;
  }

  public void init() {
    System.out.println("Init method in BeanB");
  }

  public void postProcessorInit() {
    System.out.println("BeanB postProcessor initialization");
  }

  public void destroy() {
    System.out.println("Destroy method in BeanB");
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "BeanB{" +
      "name='" + name + '\'' +
      ", value=" + value +
      '}';
  }

  @Override
  public boolean validate() {
    System.out.println("BeanB validation");
    return name != null && value > 0;
  }
}
