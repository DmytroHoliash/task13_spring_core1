package com.holiash;

import com.holiash.beans.BeanF;
import com.holiash.configs.MyConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {

  public static void main(String[] args) {
    ApplicationContext context = new AnnotationConfigApplicationContext(MyConfig.class);
    BeanF bean = context.getBean(BeanF.class);
  }
}
