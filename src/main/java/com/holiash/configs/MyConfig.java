package com.holiash.configs;

import com.holiash.beans.BeanA;
import com.holiash.beans.BeanB;
import com.holiash.beans.BeanC;
import com.holiash.beans.BeanD;
import com.holiash.beans.BeanE;
import com.holiash.beans.BeanF;
import com.holiash.beans.MyBeanFactory;
import com.holiash.beans.MyBeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;

@Configuration
@Import(BCDConfig.class)
public class MyConfig {

  @Bean
  public BeanA getBeanAa(BeanB b, BeanC c) {
    return new BeanA(b.getName(), c.getValue());
  }

  @Bean
  public BeanA getBeanAb(BeanB b, BeanD d) {
    return new BeanA(b.getName(), d.getValue());
  }

  @Bean
  public BeanA getBeanAc(BeanB c, BeanD d) {
    return new BeanA(c.getName(), d.getValue());
  }

  @Bean
  public BeanE getBeanEa(BeanA getBeanAa) {
    return new BeanE(getBeanAa.getName(), getBeanAa.getValue());
  }

  @Bean
  public BeanE getBeanEb(BeanA getBeanAb) {
    return new BeanE(getBeanAb.getName(), getBeanAb.getValue());
  }

  @Bean
  public BeanE getBeanEc(BeanA getBeanAc) {
    return new BeanE(getBeanAc.getName(), getBeanAc.getValue());
  }

  @Bean
  @Lazy
  public BeanF getBeanF() {
    return new BeanF("BeanF", 6);
  }

  @Bean(initMethod = "init")
  public MyBeanFactory getMyBeanFactory() {
    return new MyBeanFactory();
  }

  @Bean
  public MyBeanPostProcessor getMyBeanPostProcessor() {
    return new MyBeanPostProcessor();
  }
}
