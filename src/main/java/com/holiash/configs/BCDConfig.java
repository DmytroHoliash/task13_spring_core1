package com.holiash.configs;

import com.holiash.beans.BeanB;
import com.holiash.beans.BeanC;
import com.holiash.beans.BeanD;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("beans.properties")
public class BCDConfig {

  @Value("${BeanB.name}")
  String bName;
  @Value("${BeanB.value}")
  int bValue;
  @Value("${BeanC.name}")
  String cName;
  @Value("${BeanC.value}")
  int cValue;
  @Value("${BeanD.name}")
  String dName;
  @Value("${BeanD.value}")
  int dValue;

  @Bean(initMethod = "init", destroyMethod = "destroy")
  @DependsOn(value = "getBeanD")
  public BeanB getBeanB() {
    return new BeanB(bName, bValue);
  }

  @Bean(initMethod = "init", destroyMethod = "destroy")
  @DependsOn(value = "getBeanB")
  public BeanC getBeanC() {
    return new BeanC(cName, cValue);
  }

  @Bean(initMethod = "init", destroyMethod = "destroy")
  public BeanD getBeanD() {
    return new BeanD(dName, dValue);
  }
}
